﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppPedido.Models;

namespace AppPedido.Controllers
{
    public class OrderHeadersController : Controller
    {
        private Context db = new Context();

        public ActionResult AddProduct()
        {
            var categories = db.Categories.ToList();
            categories.Add(new Category
            {
                id = 0,
                CategoryName = "[Seleccione Categoria]"
            });
            ViewBag.Categoryid = new SelectList(categories.OrderBy(c=>c.id), "id", "CategoryName");

            var products = db.Products.ToList();
            products.Add(new Product
            {
                id = 0,
                ProductTitle = "[Seleccione Producto]"
            });
            ViewBag.Productid = new SelectList(products.OrderBy(c => c.id), "id", "ProductTitle");
            return View();
        }

        [HttpPost]
        public ActionResult AddProduct(AddProductView view)
        {
            int line=db.OrderDtlTemps.Count();
            var product = db.Products.Find(view.Productid);
            var OrderDtlTemp = new OrderDtlTemp
            {
                OrderDetailLine = line + 1,
                Productid = view.Productid,
                ProductNUm = product.ProductNum,
                ProductTitle = product.ProductTitle,
                UnitPrice = product.UnitPrice,
                Qty = view.Qty,
                Total = product.UnitPrice * view.Qty
            };
            db.OrderDtlTemps.Add(OrderDtlTemp);
            db.SaveChanges();
            return RedirectToAction("Create");
        }


        // GET: OrderHeaders
        public ActionResult Index()
        {
            var orderHeaders = db.OrderHeaders.Include(o => o.Customer);
            return View(orderHeaders.ToList());
        }

        // GET: OrderHeaders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderHeader orderHeader = db.OrderHeaders.Find(id);
            orderHeader.OrderDetails = db.OrderDetails.Where(o => o.OrderHeaderid == id).ToList();
            if (orderHeader == null)
            {
                return HttpNotFound();
            }
            return View(orderHeader);
        }

        // GET: OrderHeaders/Create
        public ActionResult Create()
        {
            ViewBag.Customerid = new SelectList(db.Customers, "id", "Name");
            var OrderHeader = new OrderHeader
            {
                OrderDetailsls=db.OrderDtlTemps.ToList()
            };
            return View(OrderHeader);
        }

        // POST: OrderHeaders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(OrderHeader orderHeader)
        {
            if (ModelState.IsValid)
            {
                var orderDtlTemps = db.OrderDtlTemps.ToList();
                orderHeader.Date = DateTime.Now.ToString();
                orderHeader.Total= orderDtlTemps.Sum(od => od.Total);
                db.OrderHeaders.Add(orderHeader);
                db.SaveChanges();
                
                foreach (var item in orderDtlTemps)
                {
                    var orderDetail = new OrderDetail
                    {
                        OrderDetailLine = item.OrderDetailLine,
                        OrderHeaderid = orderHeader.id,
                        Productid=item.Productid,
                        Qty=item.Qty,
                        Total=item.Total
                    };
                    db.OrderDetails.Add(orderDetail);
                    db.OrderDtlTemps.Remove(item);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Customerid = new SelectList(db.Customers, "id", "Name", orderHeader.Customerid);
            return View(orderHeader);
        }

        // GET: OrderHeaders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderHeader orderHeader = db.OrderHeaders.Find(id);
            if (orderHeader == null)
            {
                return HttpNotFound();
            }
            ViewBag.Customerid = new SelectList(db.Customers, "id", "Name", orderHeader.Customerid);
            return View(orderHeader);
        }

        // POST: OrderHeaders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Customerid,Date,Total,Observation")] OrderHeader orderHeader)
        {
            if (ModelState.IsValid)
            {
                db.Entry(orderHeader).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Customerid = new SelectList(db.Customers, "id", "Name", orderHeader.Customerid);
            return View(orderHeader);
        }

        // GET: OrderHeaders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrderHeader orderHeader = db.OrderHeaders.Find(id);
            orderHeader.OrderDetails = db.OrderDetails.Where(o => o.OrderHeaderid == id).ToList();
            if (orderHeader == null)
            {
                return HttpNotFound();
            }
            return View(orderHeader);
        }

        // POST: OrderHeaders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrderHeader orderHeader = db.OrderHeaders.Find(id);
            foreach (var item in db.OrderDetails.Where(o => o.OrderHeaderid == id).ToList())
            {
                db.OrderDetails.Remove(item);
            }
            db.OrderHeaders.Remove(orderHeader);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region JsonResult
        public JsonResult GetProducts(int Categoryid)
        {
            if(Categoryid==0)
            {
                var products2 = db.Products.ToList();
                products2.Add(new Product
                {
                    id = 0,
                    ProductTitle = "[Seleccione Producto]"
                });

                return Json(new SelectList(products2.OrderBy(c => c.id), "id", "ProductTitle"));

            }

            db.Configuration.ProxyCreationEnabled = false;
            var productcategory = db.ProductCategories.Where(p => p.id == Categoryid);

            var products = (from item in db.Products
                           join item2 in productcategory on item.id equals item2.Productid
                           select item).ToList();
            products.Add(new Product
            {
                id = 0,
                ProductTitle = "[Seleccione Producto]"
            });
            return Json(new SelectList(products.OrderBy(c => c.id), "id", "ProductTitle"));
        }
        public JsonResult GetUnitPrice(int? Productid)
        {
            if (Productid==null || Productid == 0)
            {             
                var product = new Product
                {
                    id=0,
                    ProductNum="",
                    ProductTitle="",
                    UnitPrice=0
                };
                return Json(product.UnitPrice, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var product = db.Products.Where(p => p.id == Productid).FirstOrDefault();
                return Json(product.UnitPrice, JsonRequestBehavior.AllowGet);

            }
        }
        
        #endregion
    }
}
