﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppPedido.Models
{
    public class AddProductView
    {
        [Key]
        public int id { get; set; }

        public int OrderHeaderid { get; set; }

        [Display(Name = "Categoria")]
        public int Categoryid { get; set; }

        [Display(Name = "Producto")]
        public int Productid { get; set; }

        [Display(Name = "Cantidad")]
        public decimal Qty { get; set; }

        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        [Display(Name = "Precio Unitario")]
        [DataType(DataType.Currency)]
        public decimal UnitPrice { get; set; }
    }

}