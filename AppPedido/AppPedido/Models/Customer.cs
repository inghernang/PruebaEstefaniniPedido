﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppPedido.Models
{
    public class Customer
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage = "Campo Numero Documento es obligatorio")]
        [Display(Name = "Numero Documento")]
        public string DocumentNum { get; set; }

        [Required(ErrorMessage = "Campo Nombres es obligatorio")]
        [Display(Name = "Nombres")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Campo Apellidos es obligatorio")]
        [Display(Name = "Apellidos")]
        public string LastName { get; set; }

        [Display(Name = "Telefono")]
        public string Phone { get; set; }

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public virtual ICollection<OrderHeader> OrderHeaders { get; set; }
    }
}