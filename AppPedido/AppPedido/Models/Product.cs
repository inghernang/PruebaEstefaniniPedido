﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppPedido.Models
{
    public class Product
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage = "Campo Numero Producto es obligatorio")]
        [Display(Name = "Numero Producto")]
        public string ProductNum { get; set; }

        [Required(ErrorMessage = "Campo Titulo Producto es obligatorio")]
        [Display(Name = "Titulo Producto")]
        public string ProductTitle { get; set; }

        [Required(ErrorMessage = "Campo Precio Producto es obligatorio")]
        [Display(Name = "Precio Producto")]
        public decimal UnitPrice { get; set; }

        [JsonIgnore]
        public virtual ICollection<ProductCategory> ProductCategories { get; set; }

        [JsonIgnore]
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }

    }
}