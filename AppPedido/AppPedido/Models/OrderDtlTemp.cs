﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppPedido.Models
{
    public class OrderDtlTemp
    {
        [Key]
        public int id { get; set; }

        [Display(Name = "Linea")]
        public int OrderDetailLine { get; set; }

        [Display(Name = "producto")]
        public int Productid { get; set; }

        [Display(Name = "Numero Producto")]
        public string ProductNUm { get; set; }

        [Display(Name = "Titulo Producto")]
        public string ProductTitle { get; set; }

        [Display(Name = "Precio Producto")]
        public decimal UnitPrice { get; set; }

        [Display(Name = "Cantidad")]
        public decimal Qty { get; set; }

        [Display(Name = "Total")]
        public decimal Total { get; set; }

    }
}