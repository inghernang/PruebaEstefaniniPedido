﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace AppPedido.Models
{
    public class Context:DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();// I had removed this
                                                                                /// Rest of on model creating here.
        }

        public static Context Create()
        {
            return new Context();
        }

        public System.Data.Entity.DbSet<AppPedido.Models.Category> Categories { get; set; }

        public System.Data.Entity.DbSet<AppPedido.Models.Product> Products { get; set; }

        public System.Data.Entity.DbSet<AppPedido.Models.ProductCategory> ProductCategories { get; set; }

        public System.Data.Entity.DbSet<AppPedido.Models.Customer> Customers { get; set; }

        public System.Data.Entity.DbSet<AppPedido.Models.OrderHeader> OrderHeaders { get; set; }

        public System.Data.Entity.DbSet<AppPedido.Models.AddProductView> AddProductViews { get; set; }

        public System.Data.Entity.DbSet<AppPedido.Models.OrderDtlTemp> OrderDtlTemps { get; set; }

        public System.Data.Entity.DbSet<AppPedido.Models.OrderDetail> OrderDetails { get; set; }
    }
}