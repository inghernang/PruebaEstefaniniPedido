﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppPedido.Models
{
    public class ProductCategory
    {
        [Key]
        public int id { get; set; }

        [Required(ErrorMessage = "Campo categoria es obligatorio")]
        [Display(Name = "Categoria")]
        public int Categoryid { get; set; }

        [Required(ErrorMessage = "Campo Producto es obligatorio")]
        [Display(Name = "Producto")]
        public int Productid { get; set; }

        public virtual Category Category { get; set; }
        public virtual Product Product { get; set; }
    }
}