﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AppPedido.Models
{
    public class OrderHeader
    {
        [Key]
        [ReadOnly(true)]
        [Display(Name = "OrderNum")]
        public int id { get; set; }
        
        [Required(ErrorMessage = "Campo Cliente es obligatorio")]
        [Display(Name = "Cliente")]
        public int Customerid { get; set; }

        [Display(Name = "Fecha Creacion")]
        public string Date { get; set; }

        [Display(Name = "Total")]
        public decimal Total { get; set; }
        public decimal TotalLine
        {
            get
            {
                if (OrderDetailsls == null)
                {
                    return 0;
                }

                return OrderDetailsls.Sum(od => od.Total);
            }
            set
            {
                
            }
        }


        [Display(Name = "Observation")]
        public string Observation { get; set; }

        [NotMapped]
        public List<OrderDtlTemp> OrderDetailsls { get; set; }

        public virtual Customer Customer { get; set; }

        public ICollection<OrderDetail> OrderDetails { get; set; }

    }
}