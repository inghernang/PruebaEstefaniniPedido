﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AppPedido.Models
{
    public class OrderDetail
    {
        [Key]
        public int id { get; set; }

        public int OrderHeaderid { get; set; }

        [Display(Name = "Linea")]
        public int OrderDetailLine { get; set; }

        [Display(Name = "producto")]
        public int Productid { get; set; }

        [Display(Name = "Cantidad")]
        public decimal Qty { get; set; }

        [Display(Name = "Total")]
        public decimal Total { get; set; }

        public virtual Product Product { get; set; }

        public virtual OrderHeader OrderHeader { get; set; }

    }
}